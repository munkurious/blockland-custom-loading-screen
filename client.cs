//Custom Loading Screen
//By Munkurious
//Credits to:
//Plornt
//TheBlackParrot
//Fluffy

//if you have TBP's Custom Chat add-on, it will use that font! if not, Verdana

if(isFile("config/client/chat/vars.cs"))
{
	exec("config/client/chat/vars.cs");
	$mCL::font = $Chat::FontFamily;
}
else
{
	$mCL::font = "Verdana";
}

//profiles
new GuiControlProfile(mCL_loadTextProfile)
{
	fontColor = "255 255 255 255";
	fontSize = 16;
	fontType = $mCL::font;
	justify = "Center";  
};


//GUI stuff

//--- OBJECT WRITE BEGIN ---
//background part
new GuiControl(mCL) {
	profile = "GuiDefaultProfile";
	horizSizing = "width";
	vertSizing = "height";
	position = "0 0";
	extent = getRes();
	minExtent = "8 2";
	enabled = "1";
	visible = "1";
	clipToParent = "1";

	new GuiBitmapCtrl(mCL_BG1) { 
		profile = "GuiDefaultProfile";
		horizSizing = "width";
		vertSizing = "height";
		position = "0 0";
		extent = getRes();
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		bitmap = "Add-Ons/Client_CustomLoad/images/bg/bg" @ getRandom(1,7) @ ".jpg";
		wrap = "0";
		lockAspectRatio = "0";
		alignLeft = "0";
		alignTop = "0";
		overflowImage = "0";
		keepCached = "0";
		mColor = "255 255 255 255";
		mMultiply = "0";
	};

	new GuiFadeinBitmapCtrl(mCL_BG) { 
		profile = "GuiDefaultProfile";
		horizSizing = "width";
		vertSizing = "height";
		position = "0 0";
		extent = getRes();
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		bitmap = "Add-Ons/Client_CustomLoad/images/bg/bg" @ getRandom(1,7) @ ".jpg";
		wrap = "0";
		lockAspectRatio = "0";
		alignLeft = "0";
		alignTop = "0";
		overflowImage = "0";
		keepCached = "0";
		mColor = "255 255 255 255";
		mMultiply = "0";
		pic = 1;
		waitTime = 5000;
		fadeinTime = 3000;
		fadeoutTime = 3000;
	};
};

//widget stuff
new GuiControl(mCL_top) {
	profile = "GuiDefaultProfile";
	horizSizing = "width";
	vertSizing = "height";
	position = "0 0";
	extent = getRes();
	minExtent = "8 2";
	enabled = "1";
	visible = "1";

	new GuiSwatchCtrl(mCL_top_swatch) {
		profile = "GuiDefaultProfile";
		horizSizing = "center";
		vertSizing = "center";
		position = (getWord(getRes(),0)/2)-(1000/2) SPC (getWord(getRes(),1)/2)-270;
		extent = "1000 540";
		minExtent = "600 200";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		color = "0 0 0 175";

		new GuiBitmapCtrl(mCL_top_previewImageBot) { 
			profile = "GuiDefaultProfile";
			horizSizing = "right";
			vertSizing = "bottom";
			position = "14 14";
			extent = "512 512";
			minExtent = "8 2";
			enabled = "1";
			visible = "1";
			bitmap = "Add-Ons/Client_CustomLoad/images/serverPreview.png";
			wrap = "0";
			lockAspectRatio = "0";
			alignLeft = "0";
			alignTop = "0";
			overflowImage = "0";
			keepCached = "0";
			mColor = "255 255 255 255";
			mMultiply = "0";
		};

		new GuiBitmapCtrl(mCL_top_previewImage) { 
			profile = "GuiDefaultProfile";
			horizSizing = "right";
			vertSizing = "bottom";
			position = "14 14";
			extent = "512 512";
			minExtent = "8 2";
			enabled = "1";
			visible = "1";
			bitmap = "Add-Ons/Client_CustomLoad/images/serverPreview.png";
			wrap = "0";
			lockAspectRatio = "0";
			alignLeft = "0";
			alignTop = "0";
			overflowImage = "0";
			keepCached = "0";
			mColor = "255 255 255 255";
			mMultiply = "0";
		};

		new GuiMLTextCtrl(mCL_top_serverHost) {
			profile = "GuiMLTextProfile";
			horizSizing = "right";
			vertSizing = "bottom";
			position = "540 14"; //SPC getWord(getRes(),1)-(600+(getWord(loadingProgress.extent,1))+40);
			extent = "300 30";
			minExtent = "8 2";
			visible = "1";
			lineSpacing = "2";
			allowColorChars = "0";
			maxChars = "-1";
			text = "<shadow:5:5><shadowcolor:000000><color:FFFFFF><font:"@ $mCL::font @ ":30>Host";
			maxBitmapHeight = "-1";
			selectable = "0";
		};

		new GuiMLTextCtrl(mCL_top_serverName) {
			profile = "GuiMLTextProfile";
			horizSizing = "right";
			vertSizing = "bottom";
			position = "540 44"; //SPC getWord(getRes(),1)-(600+(getWord(loadingProgress.extent,1))+40);
			extent = "800 30";
			minExtent = "8 2";
			visible = "1";
			lineSpacing = "2";
			allowColorChars = "0";
			maxChars = "-1";
			text = "<shadow:5:5><shadowcolor:000000><color:FFFFFF><font:" @ $mCL::font @ ":30>Server Name";
			maxBitmapHeight = "-1";
			selectable = "0";
		};

		new GuiMLTextCtrl(mCL_top_timeElapsed) {
			profile = "GuiMLTextProfile";
			horizSizing = "right";
			vertSizing = "bottom";
			position = "850 14"; //SPC getWord(getRes(),1)-(600+(getWord(loadingProgress.extent,1))+40);
			extent = "800 30";
			minExtent = "8 2";
			visible = "1";
			lineSpacing = "2";
			allowColorChars = "0";
			maxChars = "-1";
			text = "<just:right><shadow:5:5><shadowcolor:000000><color:FFFFFF><font:" @ $mCL::font @ ":30>0:00";
			maxBitmapHeight = "-1";
			selectable = "0";
		};

		new GuiSwatchCtrl(mCL_top_scroll_swatch) {
			profile = "GuiDefaultProfile";
			horizSizing = "right";
			vertSizing = "bottom";
			position = "540 80";
			extent = "450 410";
			minExtent = "8 2";
			enabled = "1";
			visible = "1";
			clipToParent = "1";
			color = "0 0 0 255";

			new GuiScrollCtrl(mCL_top_scroll) {
				profile = "GuiScrollProfile";
				horizSizing = "right";
				vertSizing = "bottom";
				position = "5 8";
				extent = "445 410";
				minExtent = "8 2";
				enabled = "1";
				visible = "1";
				clipToParent = "1";
				willFirstRespond = "0";
				hScrollBar = "alwaysOff";
				vScrollBar = "alwaysOff";
				constantThumbHeight = "0";
				childMargin = "0 0";
				rowHeight = "40";
				columnWidth = "30";
				mColor = "0 0 0 0";
	
				new GuiSwatchCtrl(mCL_top_scroll_expander) {
					profile = "GuiDefaultProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "1 1";
					extent = "445 410";
					minExtent = "8 2";
					enabled = "1";
					visible = "1";
					clipToParent = "1";
					color = "0 0 0 255";

					new GuiBitmapCtrl(mCL_top_scroll_canvas) {
						profile = "GuiDefaultProfile";
						horizSizing = "right";
						vertSizing = "bottom";
						position = "0 0";
						extent = "445 2000";
						minExtent = "8 2";
						enabled = "1";
						visible = "1";
						clipToParent = "1";
						bitmap = "NULL";
						wrap = "0";
						lockAspectRatio = "0";
						alignLeft = "0";
						alignTop = "0";
						overflowImage = "0";
						keepCached = "0";
						mColor = "0 0 0 255";
						mMultiply = "0";
					};
				};
			};

			new GuiSwatchCtrl(mCL_top_scroll_top) {
				profile = "GuiDefaultProfile";
				horizSizing = "right";
				vertSizing = "bottom";
				position = "6 8";
				extent = "418 20";
				minExtent = "8 2";
				enabled = "1";
				visible = "1";
				clipToParent = "1";
				color = "0 0 0 255";

				new GuiMLTextCtrl(mCL_top_scroll_top_NameText) {
					profile = "GuiMLTextProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "4 1";
					extent = "40 16";
					minExtent = "8 2";
					enabled = "1";
					visible = "1";
					clipToParent = "1";
					lineSpacing = "2";
					allowColorChars = "0";
					maxChars = "-1";
					text = "<font:" @ $mCL::font @ ":16><color:FFFFFF>Name";
					maxBitmapHeight = "-1";
					selectable = "1";
					autoResize = "1";
				};

				new GuiMLTextCtrl(mCL_top_scroll_top_IDText) {
					profile = "GuiMLTextProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "254 1";
					extent = "17 16";
					minExtent = "8 2";
					enabled = "1";
					visible = "1";
					clipToParent = "1";
					lineSpacing = "2";
					allowColorChars = "0";
					maxChars = "-1";
					text = "<font:" @ $mCL::font @ ":16><color:FFFFFF>ID";
					maxBitmapHeight = "-1";
					selectable = "1";
					autoResize = "1";
				};

				new GuiMLTextCtrl(mCL_top_scroll_top_ScoreText) {
					profile = "GuiMLTextProfile";
					horizSizing = "right";
					vertSizing = "bottom";
					position = "354 1";
					extent = "46 16";
					minExtent = "8 2";
					enabled = "1";
					visible = "1";
					clipToParent = "1";
					lineSpacing = "2";
					allowColorChars = "0";
					maxChars = "-1";
					text = "<font:" @ $mCL::font @ ":16><color:FFFFFF>Score";
					maxBitmapHeight = "-1";
					selectable = "1";
					autoResize = "1";
				};
			};
		};
	};
};

//--- OBJECT WRITE END ---


//fetching preview
//thanks to Plornt for helping me do this part
//big thanks to jincux for helping me debug stuff
function mCLGUI_PreviewTCP::online(%this,%line)
{
	//echo("%line = " @ %line);
	if(strPos(%line, "Content-Length:") == 0)
	{ 
		%this.length = getWord(%line, 1);
	}

	if(%this.getnext)
	{
		
	}

	if(%line $= "")
	{
		echo(%this.length);
		%this.setBinarySize(%this.length);
		return;
	}
}

function mCLGUI_PreviewTCP::onBinChunk(%this,%bin)
{
	//echo(%bin);
	if(%bin >= %this.length)
	{ 
		%this.savePreview(); 
	}

	//cancel(%this.timeout);
	//%this.timeout = %this.schedule(500,savePreview);
}

function mCLGUI_PreviewTCP::savePreview(%this)
{
	%this.savedPreview = true;
	%this.saveBufferToFile("config/client/mCL/mCL_ServerP.jpg");

	mCL_top_previewImage.bitmap = "config/client/mCL/mCL_serverP.jpg";
	mCL_top_swatch.remove(mCL_top_previewImage);
	mCL_top_swatch.add(mCL_top_previewImage);
}

function mCLGUI_PreviewTCP::onConnected(%this)
{
	%this.getPreview(%this.ip,%this.port);
}

function mCLGUI_PreviewTCP::getPreview(%this,%ip,%port,%blid)
{
	%uri = strreplace(%ip,".","-") @ "_" @ %port;

	%data = "q=" @ %uri @ "";

	%path = %this.path @ "?" @ %data @ " HTTP/1.1\r\n";

	%host = "Host: " @ %this.host @ "\r\n";
	%userAgent = "User-Agent: Torque/1.0\r\n";
	%type = "Content-Type: text/html\r\n";
	%headers = %host @ %userAgent @ %type;

	%req = "GET" SPC %path @ %headers @ "\n\n";

	//echo(%req);

	%this.send(%req);
	%this.binaryMode = 0;
	%this.ip = strreplace(%ip,".","-");
	%this.port = %port;
}

function mCLGUI_PreviewTCP::onDisconnect(%this)
{
	%this.delete();

	//mCL_top_previewImage.setBitmap("Add-Ons/Client_CustomLoad/images/serverPreview.png");
	//warn("lolololNO CONNECT");
}


//start connect
function mCL_sPConnect()
{
	%pre = new TCPObject(mCLGUI_PreviewTCP);
	%pre.host = "munk22.byethost9.com";
	%pre.path = "/image.blockland.us/detail.php";
	%pre.connect(%pre.host @ ":80");
	%pre.ip = serverConnection.getRawIP();
	%pre.port = mabs(getSubStr(serverConnection.getAddress(),strPos(serverConnection.getAddress(),":")+1,5));

}

//init function
function mCustomLoad_init()
{
	if(!isObject(serverConnection))
	{
		return;
	}
	
	canvas.pushDialog(mCL);
	canvas.pushDialog(mCL_top);

	loadingGui.remove(LoadingProgress);
	loadingGui.remove(LoadingSecondaryProgress);

	mCL_top_swatch.add(LoadingProgress);
	mCL_top_swatch.add(LoadingSecondaryProgress);

	LoadingProgress.resize(540,496,450,30);
	LoadingSecondaryProgress.resize(540,496,450,30);

	LoadingProgressTxt.setProfile(mCL_loadTextProfile);

	mCL_initListAdding();

	%pre = new TCPObject(mCLGUI_PreviewTCP);
	%pre.host = "munk22.byethost9.com"; 
	%pre.path = "/image.blockland.us/detail.php";
	%pre.connect(%pre.host @ ":80");
	%pre.ip = serverConnection.getRawIP();
	%pre.port = mabs(getSubStr(serverConnection.getAddress(),strPos(serverConnection.getAddress(),":")+1,5));

	loadingGui.add(newPlayerListGui);
	loadingGui.remove(newPlayerListGui);
	%title1 = NPL_Window.getValue();
	%title2 = getSubStr(%title1,strStr(%title1,"-")+2,strLen(%title1));
	%host = getSubStr(%title2,0,strstr(%title2,"'")+2);
	if(getSubStr(%title2,1,1) $= "s")
	{
		%server = getSubSTr(%title2,strstr(%title2,"'")+3,strLen(%title2));
	}
	else
	{
		%server = getSubSTr(%title2,strstr(%title2,"'")+2,strLen(%title2));
	}
	mCL_top_serverName.setText("<shadow:5:5><shadowcolor:000000><font:" @ $mCL::font @ ":30><just:left><color:ffffff> " @ %server);
	mCL_top_serverHost.setText("<shadow:5:5><shadowcolor:000000><font:" @ $mCL::font @ ":30><just:left><color:ffffff> " @ %host);

	mCL_top_timeElapsed.minute = 0;
	mCL_top_timeElapsed.second = 0;
	mCL_top_timeElapsed.loop();
}

//timeElapsed
function mCL_top_timeElapsed::loop(%this)
{
	cancel(%this.sched);

	%this.second++;
	if(%this.second >= 60)
	{
		%this.minute++;
		%this.second = 0;
	}

	%this.setText("<just:right><shadow:5:5><shadowcolor:000000><color:FFFFFF><font:" @ $mCL::font @ ":30>" @ %this.minute @ ":" @ %this.second @ "");

	%this.sched = %this.schedule(1000, loop);

}


//image switcher
function mCL_BG::onDone(%this)
{
	%this.pic++;
	%a = %this.pic;
	if(%a > 7)
	{
		%a = 1;
		%this.pic = 1;
	}
	%this.setBitmap("Add-Ons/Client_CustomLoad/images/bg/bg" @ %a @ ".jpg");
	mCL.remove(mCL_BG);
	mCL.add(mCL_BG);
}


//add players to list main
function mCL_initListAdding()
{
	mCL_top_scroll_canvas.delete();

	%i = new GuiBitmapCtrl(mCL_top_scroll_canvas) {
		profile = "GuiDefaultProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "0 0";
		extent = "440 2000";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		bitmap = "NULL";
		wrap = "0";
		lockAspectRatio = "0";
		alignLeft = "0";
		alignTop = "0";
		overflowImage = "0";
		keepCached = "0";
		mColor = "0 0 0 255";
		mMultiply = "0";
	};

	mCL_top_scroll_expander.add(%i);


	$mCL::currID = 0;

	mCL_top.Add(NewPlayerListGui);
	mCL_top.remove(NewPlayerListGui);

	//player list
	%rows = NPL_List.rowCount();
	for(%i = 0; %i < %rows; %i++)
	{
		%line = NPL_List.getRowText(%i);
		%var4 = getField(%line, 3); 
		%var2 = getField(%line, 1);
		%var3 = getField(%line, 2);
		%var5 = getField(%line, 4);
		%var6 = getField(%line, 0);

		mCL_addPlayerToList(%var2, %var4, %var3);
	}
}


//add players to the list
function mCL_addPlayerToList(%name, %BL_ID, %score)
{
	$mCL::currID++;

	%yPos = 18 * $mCL::currID;
	%yheight = 18 * ($mCL::currID + 2);

	%color = "FFFFFF";

	if(%name $= $pref::Player::NetName)
	{
		%color = "FFA500";
	}
	
	%a = new GuiMLTextCtrl() {
		profile = "GuiMLTextProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "3" SPC %yPos;
		extent = "239 18";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		lineSpacing = "2";
		allowColorChars = "0";
		maxChars = "-1";
		text = "<font:" @ $mCL::font @ ":16><color:" @ %color @ ">" @ %name;
		maxBitmapHeight = "-1";
		selectable = "1";
		autoResize = "1";
	};

	%b = new GuiMLTextCtrl() {
		profile = "GuiMLTextProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "253" SPC %yPos;
		extent = "94 18";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		lineSpacing = "2";
		allowColorChars = "0";
		maxChars = "-1";
		text = "<font:" @ $mCL::font @ ":16><color:" @ %color @ ">" @ %BL_ID;
		maxBitmapHeight = "-1";
		selectable = "1";
		autoResize = "1";
	};

	%c = new GuiMLTextCtrl() {
		profile = "GuiMLTextProfile";
		horizSizing = "right";
		vertSizing = "bottom";
		position = "354" SPC %yPos;
		extent = "94 18";
		minExtent = "8 2";
		enabled = "1";
		visible = "1";
		clipToParent = "1";
		lineSpacing = "2";
		allowColorChars = "0";
		maxChars = "-1";
		text = "<font:" @ $mCL::font @ ":16><color:" @ %color @ ">" @ %score;
		maxBitmapHeight = "-1";
		selectable = "1";
		autoResize = "1";
	};
		
	
	mCL_top_scroll_canvas.add(%a);
	mCL_top_scroll_canvas.add(%b);
	mCL_top_scroll_canvas.add(%c);
	
	if(getWord(mCL_top_scroll_canvas.extent, 1) < %yheight)
	{
		mCL_top_scroll_canvas.resize(1, 1, 418, %yheight);
	}

}



//package
package mCustomLoad
{
	function LoadingGui::onWake( %this, %a )
	{
		mCustomLoad_init();
		parent::onWake( %this, %a );
	}

	//when someone joins the server you're on
	function secureClientCmd_clientJoin( %this, %a, %b, %c, %d, %e, %f, %g )
	{
		parent::secureClientCmd_clientJoin( %this, %a, %b, %c, %d, %e, %f, %g );
		mCL_initListAdding();
	}

	function secureClientCmd_clientDrop( %this, %a, %b, %c, %d, %e, %f, %g )
	{
		parent::secureClientCmd_clientDrop( %this, %a, %b, %c, %d, %e, %f, %g );
		mCL_initListAdding();
	}
};
activatePackage( mCustomLoad );